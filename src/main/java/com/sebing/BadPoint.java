package com.sebing;

/**
 * This class does not override {@link Object#hashCode()}
 */
class BadPoint {
    private final int x;
    private final int y;

    BadPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadPoint badPoint = (BadPoint) o;
        if (x != badPoint.x) return false;
        return y == badPoint.y;
    }
}
