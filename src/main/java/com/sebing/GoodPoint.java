package com.sebing;

/**
 * This class overrides {@link Object#hashCode()}
 */
class GoodPoint {
    private final int x;
    private final int y;

    GoodPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodPoint goodPoint = (GoodPoint) o;
        if (x != goodPoint.x) return false;
        return y == goodPoint.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
