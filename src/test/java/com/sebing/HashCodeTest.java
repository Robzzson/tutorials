package com.sebing;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HashCodeTest {

    private HashSet<GoodPoint> goodPointHashSet = new HashSet<>();
    private HashSet<BadPoint> badPointHashSet = new HashSet<>();
    private GoodPoint objectScopeGoodPoint = new GoodPoint(6,8);
    private BadPoint objectScopeBadPoint = new BadPoint(6,8);

    @Test
    public void hashCodeTest() {
        goodPointHashSet.add(new GoodPoint(2,4));
        badPointHashSet.add(new BadPoint(2,4));
        goodPointHashSet.add(objectScopeGoodPoint);
        badPointHashSet.add(objectScopeBadPoint);
        assertTrue(goodPointHashSet.contains(new GoodPoint(2,4)));
        assertFalse(badPointHashSet.contains(new BadPoint(2,4)));
        assertTrue(goodPointHashSet.contains(objectScopeGoodPoint));
        assertTrue(badPointHashSet.contains(objectScopeBadPoint));
    }
}
